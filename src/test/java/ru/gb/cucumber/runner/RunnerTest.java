package ru.gb.cucumber.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = {"src/test/resources/features"},
        glue = {"ru/gb/cucumber/steps", "ru/gb/cucumber/hooks"}
)
public class RunnerTest extends AbstractTestNGCucumberTests {
}
