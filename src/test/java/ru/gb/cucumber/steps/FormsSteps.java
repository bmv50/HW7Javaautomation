package ru.gb.cucumber.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import ru.gb.base.BaseTest;
import ru.gb.pages.FormsPage;
import ru.gb.pages.LoginPage;
import ru.gb.pages.MainPage;

public class FormsSteps extends BaseTest {
    MainPage mainPage;
    FormsPage formsPage;


    @Given("^User is on Forms page$")
    public void user_is_on_Forms_page()  {
        formsPage = openApp().clickFormsButton();
    }

    @When("^User click switch button$")
    public void user_click_login_button() {
        formsPage.clickSwitchButton();
    }

    @Then("^User should see switch text \"([^\"]*)\"$")
    public void user_should_see_error_message(String arg1)  {
        formsPage.checkSwitchText(arg1);
    }
}
