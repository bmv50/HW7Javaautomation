package ru.gb.pages;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import ru.gb.locators.LoginPageLocators;

import static com.codeborne.selenide.Selenide.$;

public class LoginPage {

    // Метод позволяет работать с локаторами для нужной нам страницы.
    private LoginPageLocators locator() {
        return new LoginPageLocators();
    }

    @Step("Вводим в поле 'mail' электронную почту")
    public LoginPage inputEmail() {
        $(locator().loginField()).sendKeys("test@test.com");
        return new LoginPage();
    }

    @Step("Вводим в поле 'password' пароль")
    public LoginPage inputPass() {
        $(locator().passField()).sendKeys("qwer1234");
        return new LoginPage();
    }

    @Step("Кликаем по кнопке 'логин' в форме")
    public LoginPage clickLoginButton() {
        $(locator().loginButton()).click();
        return new LoginPage();
    }

    @Step("Проверяем текст ошибки")
    public LoginPage checkLoginErrorText(String text) {
        $(locator().loginErrorText()).shouldHave(Condition.text(text));
        return new LoginPage();
    }

    @Step("Проверяем текст успешного входа")
    public LoginPage checkLoginText(String text) {
        $(locator().loginText()).shouldHave(Condition.text(text));
        return new LoginPage();
    }
}


