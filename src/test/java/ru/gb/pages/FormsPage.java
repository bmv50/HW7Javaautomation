package ru.gb.pages;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import ru.gb.locators.FormsPageLocators;

import static com.codeborne.selenide.Selenide.$;

public class FormsPage {

    private FormsPageLocators locator() {
        return new FormsPageLocators();
    }

    @Step("Кликаем по switch и меняем его состояние")
    public FormsPage clickSwitchButton() {
        $(locator().formsSwitch()).click();
        return new FormsPage();
    }

    @Step("Проверяем текст сообщения об изменении статуса switch")
    public FormsPage checkSwitchText(String text) {
        $(locator().statusSwitchText()).shouldHave(Condition.text(text));
        return new FormsPage();
    }
}
