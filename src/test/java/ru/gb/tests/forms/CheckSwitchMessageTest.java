package ru.gb.tests.forms;

import io.qameta.allure.Description;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.gb.base.BaseTest;
import ru.gb.listeners.AllureListener;

@Listeners(AllureListener.class)
public class CheckSwitchMessageTest extends BaseTest {

    public static final String VALID_MESSAGE = "Click to turn the switch OFF";

    @Test
    @Description("Проверяем сообщение о переключении switch")
    public void CheckEmptySwitch() {
        openApp()
                .clickFormsButton()
                .clickSwitchButton()
                .checkSwitchText(VALID_MESSAGE);
    }

}
