package ru.gb.locators;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class LoginPageLocators {
    public By loginField() {return MobileBy.AccessibilityId("input-email");}

    public By passField() {return MobileBy.AccessibilityId("input-password");}

    public By loginButton() {return MobileBy.AccessibilityId("button-LOGIN");}

    public By loginText() {return MobileBy.AccessibilityId("android:id/message");}

    public By loginErrorText() {
        return MobileBy.xpath("//android.widget.ScrollView[@content-desc=\"Login-screen\"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView");
    }

}
