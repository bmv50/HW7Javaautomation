package ru.gb.locators;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class FormsPageLocators {

    public By formsSwitch() {return MobileBy.AccessibilityId("switch");}

    public By statusSwitchText() {return MobileBy.AccessibilityId("switch-text");}
}
