Feature: Login

  @ValidCredentials
  Scenario: Login without password

    Given User is on Login page
    When User input email
    And User click login button
    Then User should see error message "Please enter at least 8 characters"