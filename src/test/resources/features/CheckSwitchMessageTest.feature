Feature: Forms

  @ValidCredentials
  Scenario: Switch radiobutton

    Given User is on Forms page
    When User click switch button
    Then User should see switch text "Click to turn the switch OFF"